---
layout: page
title: About
permalink: /about/
---

To know more about me visit: fayazrasheed.com

You can find the source code for the Donations-challengex at:
{% include icon-github.html username="fayazr" %} /
[Donations-challengex](https://github.com/fayazr)
