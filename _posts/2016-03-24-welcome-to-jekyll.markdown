---
layout: post
title:  "Welcome to the first post!"
date:   2018-03-24 15:32:14 -0300
categories: jekyll update
---
You’ll find this post in your `_posts` directory.


If you have any questions please ask!

A sample code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Find my github page at [Fayaz’s GitHub repo][fayaz-gh]. 


[fayaz-gh]:   https://github.com/fayazr

